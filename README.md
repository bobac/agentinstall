# README #

Installs MAX RM Agent on Ubuntu Debian 14.04 LTS

### How do I get set up? ###

* expects wget installed, if not
```
 apt-get install wget -y
```
* grab repo & run the script
```
git clone git@bitbucket.org:bobac/agentinstall.git
cd agentinstall
./agent-install.sh
```
* chmod +x agent-install.sh if necessary
* thats all folks :-)