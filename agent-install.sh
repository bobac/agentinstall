#!/bin/sh
# repo: http://repos.systemmonitor.eu.com/rmmagent/xUbuntu_13.04/
# You must be sudo to run this

cp /etc/apt/sources.list /etc/apt/sources.list.orig
echo "deb http://repos.systemmonitor.eu.com/rmmagent/xUbuntu_13.04/ ./" >> /etc/apt/sources.list
wget http://repos.systemmonitor.eu.com/rmmagent/xUbuntu_13.04/Release.key
apt-key add Release.key
apt-get update
apt-get install rmmagent sysv-rc-conf -y
cd /usr/local/rmmagent
./rmmagentd -i
sysv-rc-conf rmmagent on

